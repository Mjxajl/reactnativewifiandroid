/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {PermissionsAndroid} from 'react-native';
import wifi from 'react-native-android-wifi';
async function getWifiPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Wifi networks',
        message: 'We need your permission in order to find wifi networks',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('Thank you for your permission! :)');
    } else {
      console.log('You will not able to retrieve wifi available networks list');
    }
  } catch (err) {
    console.warn(err);
  }
}
const App: () => React$Node = () => {
  console.log(
    'FFFFFFFEEEEEEEEEEEEEEEEEETTTTTTTTTTTTTTTTCCCCCCCCCCCHHHHHHHHHHHHHHHHHHH',
  );
  fetch('http://quadrobit.com/')
    .then(response => console.log(response))
    .catch(e => console.log(e));
  getWifiPermission();
  wifi.isEnabled(isEnabled => {
    if (isEnabled) {
      console.log('wifi service enabled@@@@@@');
    } else {
      console.log('wifi service is disabled');
    }
  });
  wifi.getSSID(ssid => {
    console.log(ssid);
  });
  wifi.loadWifiList(
    wifiStringList => {
      var wifiArray = JSON.parse(wifiStringList).filter(network =>
        network.SSID.includes('QUADROBASE'),
      );
      console.log(wifiArray);
      wifi.findAndConnect(wifiArray[0]['SSID'], '00000000', found => {
        if (found) {
          const responces = setInterval(function() {
            var details = {
              sta_ssid: 'DAN.IT-College',
              sta_password: 'Secur567',
              sta_type: 'dhcp',
              mode: 'sta',
            };

            var formBody = [];
            for (var property in details) {
              var encodedKey = encodeURIComponent(property);
              var encodedValue = encodeURIComponent(details[property]);
              formBody.push(encodedKey + '=' + encodedValue);
            }
            formBody = formBody.join('&');

            fetch('http://192.168.4.1/wifi/apply.cgi', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
              },
              body: formBody,
            })
              .then(result => clearInterval(responces))
              .catch(e => console.log(e));
          }, 333);
        } else {
          console.log('wifi is not in range');
        }
      });
    },
    error => {
      console.log(error);
    },
  );

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <Header />
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Step One</Text>
              <Text style={styles.sectionDescription}>
                Edit <Text style={styles.highlight}>App.js</Text> to change this
                screen and then come back to see your edits.
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>See Your Changes</Text>
              <Text style={styles.sectionDescription}>
                <ReloadInstructions />
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Debug</Text>
              <Text style={styles.sectionDescription}>
                <DebugInstructions />
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Learn More</Text>
              <Text style={styles.sectionDescription}>
                Read the docs to discover what to do next:
              </Text>
            </View>
            <LearnMoreLinks />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
